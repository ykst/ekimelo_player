import os
import time
import tkinter
import glob
import threading
import random
from tkinter import *
from tkinter.ttk import *

from funcs import *

try:
    import RPi.GPIO as GPIO
except Exception as e:
    print(e)


size_btn_h = 4
size_btn_w = size_btn_h*2
size_font_h1 = 24
size_font_h2 = 16
size_font_h4 = 12
size_frame3l_h = 400
size_frame3l_w = 1000
size_frame3l_tab2_box_h = 18
size_frame3l_tab2_box_w = 100
size_frame3l_tab3_box_h = 18
size_frame3l_tab3_box_w = 50
size_frame3r_h = size_frame3l_h
size_frame3r_w = size_frame3l_w

cl_active = '#ffffff'
cl_passive = '#c0c0c0'



class Window(tkinter.Frame):
    def __init__(self, master=None):
        tkinter.Frame.__init__(self, master)
        self.master.title('Raspiで発車ベルを鳴らす')

        self.hoge = 0
        self.int_var = tkinter.IntVar(0, 10)
        self.th_gpio = None

        self.cls_managecsv = ManageCsv()
        self.cls_managecsv.csv_read('./ekilist.csv')

        self.mel = SoundManager()
        self.ann = SoundManager()
        self.env = EnvManager()
        self.env.set_list_path(
            glob.glob('./ekimelo/**/*.wav', recursive=True),
            glob.glob('./announce/**/*.wav', recursive=True))

        self.str_now_mel = tkinter.StringVar(0, "選択してください")
        self.str_now_ann = tkinter.StringVar(0, "選択してください")
        self.str_now_sta = tkinter.StringVar(0, "-")
        # self.lst_corp = tkinter.StringVar(value=[])
        # self.lst_line = tkinter.StringVar(value=[])
        # self.lst_sta = tkinter.StringVar(value=[])
        # self.lst_track = tkinter.StringVar(value=[])
        self.lst_glp_mel_ann = tkinter.StringVar(value=self.cls_managecsv.get_glp())
        self.lst_ann = tkinter.StringVar(value=self.cls_managecsv.get_name_ann())
        self.lst_mel = tkinter.StringVar(value=self.cls_managecsv.get_name_mel())

        self.flg_loop = tkinter.BooleanVar(0, True)
        self.flg_rand = tkinter.BooleanVar(0, True)
        self.flg_swopt = tkinter.BooleanVar(0, False)
        self.status_sw = tkinter.BooleanVar(0, False)
        self.path_mel = tkinter.StringVar(0, None)
        self.path_ann = tkinter.StringVar(0, None)
        self.int_wait_mel = tkinter.IntVar(0, 0)
        self.int_wait_ann = tkinter.IntVar(0, 0)
        self.time_rand_timer = time.time()
        self.rand_path_mel = None
        self.rand_path_ann = None

        # どのタブにいるか確認する用 #TAB1 : 1,
        self.int_tab_no = tkinter.IntVar(0, 0)

        self.frame_1 = tkinter.Frame(self, relief=tkinter.RIDGE, bd=4)
        self.frame_1.pack(fill=tkinter.X, expand=1)
        self.frame_1_l = tkinter.Frame(self.frame_1)
        self.frame_1_l.pack(side=tkinter.LEFT, fill="x")
        self.lbl_title = tkinter.Label(
            self.frame_1_l, text='Raspiで発車ベルを鳴らす', font=("", size_font_h1))
        self.lbl_title.grid(row=0, column=0)

        self.frame_2 = tkinter.Frame(self, relief=tkinter.RIDGE, bd=4)
        self.frame_2.pack(fill=tkinter.X, expand=1)

        self.frame_1_r = tkinter.Frame(self.frame_1)
        self.frame_1_r.pack(side=tkinter.RIGHT, fill="x")

        self.lbl_disp_mel = tkinter.Label(
            self.frame_1_r, text='メロディー', font=("", size_font_h2))
        self.lbl_disp_mel.grid(row=1, column=0)
        self.lbl_now_mel = tkinter.Label(
            self.frame_1_r, textvariable=self.str_now_mel, font=("", size_font_h2))
        self.lbl_now_mel.grid(row=1, column=1)
        self.lbl_disp_ann = tkinter.Label(
            self.frame_1_r, text='アナウンス', font=("", size_font_h2))
        self.lbl_disp_ann.grid(row=2, column=0)
        self.lbl_now_ann = tkinter.Label(
            self.frame_1_r, textvariable=self.str_now_ann, font=("", size_font_h2))
        self.lbl_now_ann.grid(row=2, column=1)
        self.lbl_disp_sta = tkinter.Label(
            self.frame_1_r, text='使用駅', font=("", size_font_h2))
        self.lbl_disp_sta.grid(row=3, column=0)
        self.lbl_now_sta = tkinter.Label(
            self.frame_1_r, textvariable=self.str_now_sta, font=("", size_font_h2))
        self.lbl_now_sta.grid(row=3, column=1)
        # self.lbl_disp_sta = tkinter.Label(
        #     self.frame_1_r, text='DEBUG', font=("", size_font_h2))
        # self.lbl_disp_sta.grid(row=4, column=0)
        # self.lbl_now_sta = tkinter.Label(
        #     self.frame_1_r, textvariable=self.status_sw, font=("", size_font_h2))
        # self.lbl_now_sta.grid(row=4, column=1)

        self.frame_3 = tkinter.Frame(self, relief=tkinter.RIDGE, bd=4)
        self.frame_3.pack(fill=tkinter.X, expand=1)

        self.frame_3_l = tkinter.Frame(self.frame_3)
        self.frame_3_l.grid(row=0, column=0)

        # タブ初期化
        self.reflesh_tab()
        self.active_tab_0()

        self.frame_3_r = tkinter.LabelFrame(
            self.frame_3, width=size_frame3r_w, height=size_frame3r_h)
        self.frame_3_r.grid(row=0, column=1)

        self.frame_3_r_bdy = tkinter.LabelFrame(
            self.frame_3_r, width=size_frame3l_w, height=size_frame3l_h)
        self.frame_3_r_bdy.grid(row=1, column=0)

        self.btn_on = tkinter.Button(
            self.frame_3_r_bdy, text='ON',font=("", size_font_h4), command=self.set_sw_on, width=size_btn_w, height=size_btn_h)
        self.btn_on.grid(row=0, column=0)
        self.btn_off = tkinter.Button(
            self.frame_3_r_bdy, text='OFF',font=("", size_font_h4), command=self.set_sw_off, width=size_btn_w, height=size_btn_h)
        self.btn_off.grid(row=1, column=0)

        # ボタン初期化
        self.passive_sw()

        self.frame_3_r_bdy.grid_columnconfigure(0, weight=1)
        self.frame_3_r_bdy.grid_rowconfigure(0, weight=1)

        self.frame_4 = tkinter.Frame(self, relief=tkinter.RIDGE, bd=4)
        self.frame_4.pack(fill=tkinter.X, expand=1)

        self.frame_4_l = tkinter.Frame(self.frame_4)
        self.frame_4_l.pack(side=tkinter.LEFT, fill=tkinter.X, expand=1)

        self.frame_4_l_1 = tkinter.LabelFrame(self.frame_4_l, text='ランダム')
        self.frame_4_l_1.grid(row=0, column=0)
        self.rdb_flg_rand_opt1 = tkinter.ttk.Radiobutton(
            self.frame_4_l_1,
            text='TRUE',
            value=True,
            variable=self.flg_rand,
            command=self.active_tab_0,
        )
        self.rdb_flg_rand_opt1.pack()
        self.rdb_flg_rand_opt2 = tkinter.ttk.Radiobutton(
            self.frame_4_l_1,
            text='FALSE',
            value=False,
            variable=self.flg_rand,
            command=self.active_tab_1,
            )
        self.rdb_flg_rand_opt2.pack()

        self.frame_4_l_2 = tkinter.LabelFrame(self.frame_4_l, text='ループ')
        self.frame_4_l_2.grid(row=0, column=1)
        self.rdb_flg_loop_opt1 = tkinter.ttk.Radiobutton(
            self.frame_4_l_2,
            text='TRUE',
            value=True,
            variable=self.flg_loop,
        )
        self.rdb_flg_loop_opt1.pack()
        self.rdb_flg_loop_opt2 = tkinter.ttk.Radiobutton(
            self.frame_4_l_2,
            text='FALSE',
            value=False,
            variable=self.flg_loop,
        )
        self.rdb_flg_loop_opt2.pack()

        self.frame_4_l_3 = tkinter.LabelFrame(self.frame_4_l, text='SW接続')
        self.frame_4_l_3.grid(row=0, column=2)
        self.rdb_flg_swopt_opt1 = tkinter.ttk.Radiobutton(
            self.frame_4_l_3,
            text='Active',
            value=True,
            variable=self.flg_swopt,
            command=self.active_sw,
        )
        self.rdb_flg_swopt_opt1.pack()
        self.rdb_flg_swopt_opt2 = tkinter.ttk.Radiobutton(
            self.frame_4_l_3,
            text='Passive',
            value=False,
            variable=self.flg_swopt,
            command=self.passive_sw,
        )
        self.rdb_flg_swopt_opt2.pack()

        self.frame_4_l_3 = tkinter.LabelFrame(self.frame_4_l, text='待ち時間')
        self.frame_4_l_3.grid(row=0, column=3)
        self.lbl_wait_mel = tkinter.Label(self.frame_4_l_3, text='メロディー (秒)')
        self.lbl_wait_mel.grid(row=0, column=0)
        self.lbl_wait_mel = tkinter.Entry(
            self.frame_4_l_3, textvariable=self.int_wait_mel)
        self.lbl_wait_mel.grid(row=0, column=1)
        self.lbl_wait_ann = tkinter.Label(self.frame_4_l_3, text='アナウンス (秒)')
        self.lbl_wait_ann.grid(row=1, column=0)
        self.lbl_wait_ann = tkinter.Entry(
            self.frame_4_l_3, textvariable=self.int_wait_ann)
        self.lbl_wait_ann.grid(row=1, column=1)
        self.frame_4_r = tkinter.Frame(self.frame_4)
        self.frame_4_r.pack(side=tkinter.RIGHT,
                            fill=tkinter.X, anchor=tkinter.SE)
        self.btn_exit = tkinter.Button(
            self.frame_4_r, text='EXIT', command=lambda: self.esc(),)
        self.btn_exit.pack()



        self.raspi()
        # self.master.attributes("-fullscreen", True)
        # self.master.attributes("-topmost", True)
        self.master.configure(bg="white")

    def set_mer_ann(self):
        if(self.int_tab_no.get() == 0):
            if(time.time()-self.time_rand_timer>10 or self.rand_path_mel==None or self.rand_path_ann==None ):
                self.rand_path_mel=self.env.list_path_melody[random.randint(0,len(self.env.list_path_melody)-1)]
                self.rand_path_ann=self.env.list_path_announce[random.randint(0,len(self.env.list_path_announce)-1)]
            self.time_rand_timer=time.time()
            self.str_now_mel.set(self.rand_path_mel)
            self.str_now_ann.set(self.rand_path_ann)
            self.str_now_sta.set("----")
            self.path_mel.set(self.rand_path_mel)
            self.path_ann.set(self.rand_path_ann)


        if(self.int_tab_no.get() == 1):
            self.str_now_mel.set(self.cmb_path_mel.get())
            self.str_now_ann.set(self.cmb_path_ann.get())
            self.str_now_sta.set("----")
            self.path_mel.set(self.cmb_path_mel.get())
            self.path_ann.set(self.cmb_path_ann.get())

        if(self.int_tab_no.get() == 2):
            
            if len(self.lst_tab_2_glp.curselection())==0:
                return
            ind = self.lst_tab_2_glp.curselection()[0]
            
            self.str_now_mel.set(self.cls_managecsv.get_mel_name_by_index(ind))
            self.str_now_ann.set(self.cls_managecsv.get_ann_name_by_index(ind))
            self.str_now_sta.set(self.cls_managecsv.get_sta_name_by_index(ind))
            self.path_mel.set(self.cls_managecsv.get_mel_path_by_index(ind))
            self.path_ann.set(self.cls_managecsv.get_ann_path_by_index(ind))

        if(self.int_tab_no.get() == 3):
            if len(self.lst_tab_3_mel.curselection())==0:
                return
            ind_mel = self.lst_tab_3_mel.curselection()[0]
            if len(self.lst_tab_3_ann.curselection())==0:
                return
            ind_ann = self.lst_tab_3_ann.curselection()[0]
            self.str_now_mel.set(self.cls_managecsv.get_mel_name_by_index(ind_mel))
            self.str_now_ann.set(self.cls_managecsv.get_ann_name_by_index(ind_ann))
            self.str_now_sta.set("----")
            self.path_mel.set(self.cls_managecsv.get_mel_path_by_index(ind_mel))
            self.path_ann.set(self.cls_managecsv.get_ann_path_by_index(ind_ann))

    def reflesh_disp(self):
        pass

    def set_sw_on(self):
        self.status_sw.set(True)
        self.btn_on.configure(state=tkinter.DISABLED)
        self.btn_off.configure(state=tkinter.NORMAL)
        # print(self.status_sw.get())
        self.set_mer_ann()

        self.env.set_sound(
            self.path_mel.get(),
            None,
            self.path_ann.get(),
            None)

        self.env.set_env(
            self.int_wait_mel.get(),
            self.flg_loop.get(),
            self.int_wait_ann.get())

        if(self.path_mel.get()!=""):        
            self.mel.stop_play()
            self.mel.add_file(self.env.melody_path)
            self.mel.start_play(self.env.melody_wait, self.env.melody_loop)

    def set_sw_off(self):
        self.time_rand_timer=time.time()
        self.status_sw.set(False)
        self.btn_on.configure(state=tkinter.NORMAL)
        self.btn_off.configure(state=tkinter.DISABLED)
        # print(self.status_sw.get())
        # self.set_mer_ann()
        if(self.path_ann.get()!=""):
            self.mel.stop_play()
            self.ann.stop_play()
            self.ann.add_file(self.env.announce_path)
            self.ann.start_play(self.env.announce_wait, self.env.announce_loop)

    def active_sw(self):
        self.btn_on.configure(state=tkinter.DISABLED)
        self.btn_off.configure(state=tkinter.DISABLED)
        self.set_sw_off()

    def passive_sw(self):
        self.btn_on.configure(state=tkinter.NORMAL)
        self.btn_off.configure(state=tkinter.NORMAL)
        print('in passive')
        print(self.path_ann.get())
        self.set_sw_off()


    def esc(self):
        self.mel.stop_play()
        self.ann.stop_play()
        self.destroy()
        self.quit()
        if(self.th_gpio != None):
            self.th_gpio._stop

    def reflesh_tab(self):
        self.frame_3_l_tab = tkinter.Frame(self.frame_3_l)
        self.frame_3_l_tab.grid(row=0, column=0, sticky=tkinter.W)
        self.tab_1 = tkinter.Button(
            self.frame_3_l_tab, text='パス指定', command=self.active_tab_1,font=("", size_font_h4))
        self.tab_1.grid(row=0, column=0,)
        self.tab_2 = tkinter.Button(
            self.frame_3_l_tab, text='駅指定', command=self.active_tab_2,font=("", size_font_h4))
        self.tab_2.grid(row=0, column=1)
        self.tab_3 = tkinter.Button(
            self.frame_3_l_tab, text='メロディー / アナウンス指定', command=self.active_tab_3,font=("", size_font_h4))
        self.tab_3.grid(row=0, column=2)
        self.tab_1.configure(background=cl_passive)
        self.tab_2.configure(background=cl_passive)
        self.tab_3.configure(background=cl_passive)
        # self.tab_4.configure(background=cl_passive)
        self.frame_3_l_bdy = tkinter.LabelFrame(
            self.frame_3_l, width=size_frame3l_w, height=size_frame3l_h)
        self.frame_3_l_bdy.configure(background=cl_active)
        self.frame_3_l_bdy.grid(row=1, column=0)

    def active_tab_0(self):
        self.int_tab_no.set(0)
        self.reflesh_tab()
        self.tab_1.configure(background=cl_passive, state=tkinter.DISABLED)
        self.tab_2.configure(background=cl_passive, state=tkinter.DISABLED)
        self.tab_3.configure(background=cl_passive, state=tkinter.DISABLED)

        self.frame_3_l_bdy = tkinter.LabelFrame(
            self.frame_3_l, width=size_frame3l_w, height=size_frame3l_h)
        self.frame_3_l_bdy.configure(background=cl_passive)
        self.frame_3_l_bdy.grid(row=1, column=0)

        self.lbl_tab_0 = tkinter.Label(
            self.frame_3_l_bdy, text='ランダム再生がONになっています',font=("", size_font_h4))
        self.lbl_tab_0.grid(row=0, column=0)

    def active_tab_1(self):
        self.int_tab_no.set(1)
        self.reflesh_tab()

        self.tab_1.configure(background=cl_active)

        self.frame_3_l_bdy = tkinter.LabelFrame(
            self.frame_3_l, width=size_frame3l_w, height=size_frame3l_h)
        self.frame_3_l_bdy.configure(background=cl_active)
        self.frame_3_l_bdy.grid(row=1, column=0)

        self.lbl_tab_1_path_mel = tkinter.Label(
            self.frame_3_l_bdy, text='メロディー',font=("", size_font_h4))
        self.lbl_tab_1_path_mel.grid(row=0, column=0)

        self.cmb_path_mel = tkinter.ttk.Combobox(
            self.frame_3_l_bdy, width=100, state='readonly')
        self.cmb_path_mel["values"] = (self.env.list_path_melody)
        self.cmb_path_mel.current(0)
        self.cmb_path_mel.grid(row=0, column=1)

        self.lbl_tab_1_path_mel = tkinter.Label(
            self.frame_3_l_bdy, text='アナウンス',font=("", size_font_h4))
        self.lbl_tab_1_path_mel.grid(row=1, column=0)

        self.cmb_path_ann = tkinter.ttk.Combobox(
            self.frame_3_l_bdy, width=100, state='readonly')
        self.cmb_path_ann["values"] = (self.env.list_path_announce)
        self.cmb_path_ann.current(0)
        self.cmb_path_ann.grid(row=1, column=1)

    def active_tab_2(self):
        self.int_tab_no.set(2)
        self.reflesh_tab()

        self.tab_2.configure(background=cl_active)

        self.frame_3_l_bdy = tkinter.LabelFrame(
            self.frame_3_l, width=size_frame3l_w, height=size_frame3l_h)
        self.frame_3_l_bdy.configure(background=cl_active)
        self.frame_3_l_bdy.grid(row=1, column=0)

        self.lbl_tab_2_glp = tkinter.Label(self.frame_3_l_bdy, text='グループ',font=("", size_font_h4))
        self.lbl_tab_2_glp.grid(row=0, column=0)
        self.frame_tab_2_glp = tkinter.Frame(self.frame_3_l_bdy)
        self.frame_tab_2_glp.grid(row=1, column=0)
        self.lst_tab_2_glp = tkinter.Listbox(
            self.frame_tab_2_glp, listvariable=self.lst_glp_mel_ann, width=size_frame3l_tab2_box_w, height=size_frame3l_tab2_box_h)
        self.lst_tab_2_glp.pack(side=tkinter.LEFT, fill="y")
        self.scroll_lst_tab_2_glp = tkinter.Scrollbar(
            self.frame_tab_2_glp, orient=tkinter.VERTICAL, command=self.lst_tab_2_glp.yview)
        self.scroll_lst_tab_2_glp.pack(side=tkinter.RIGHT, fill="y")
        self.lst_tab_2_glp["yscrollcommand"] = self.scroll_lst_tab_2_glp.set
        # self.btn_tab_2_corp = tkinter.Button(self.frame_3_l_bdy, text='登録')
        # self.btn_tab_2_corp.configure(state=tkinter.DISABLED)
        # self.btn_tab_2_corp.grid(row=3, column=0)

    #     self.lbl_tab_2_corp = tkinter.Label(self.frame_3_l_bdy, text='会社名')
    #     self.lbl_tab_2_corp.grid(row=0, column=0)
    #     self.frame_tab_2_corp = tkinter.Frame(self.frame_3_l_bdy)
    #     self.frame_tab_2_corp.grid(row=1, column=0)
    #     self.lst_tab_2_corp = tkinter.Listbox(
    #         self.frame_tab_2_corp, listvariable=self.lst_corp)
    #     self.lst_tab_2_corp.pack(side=tkinter.LEFT, fill="y")
    #     self.scroll_lst_tab_2_corp = tkinter.Scrollbar(
    #         self.frame_tab_2_corp, orient=tkinter.VERTICAL, command=self.lst_tab_2_corp.yview)
    #     self.scroll_lst_tab_2_corp.pack(side=tkinter.RIGHT, fill="y")
    #     self.lst_tab_2_corp["yscrollcommand"] = self.scroll_lst_tab_2_corp.set
    #     self.btn_tab_2_corp = tkinter.Button(self.frame_3_l_bdy, text='絞り込み')
    #     self.btn_tab_2_corp.configure(state=tkinter.DISABLED)
    #     self.btn_tab_2_corp.grid(row=3, column=0)

    #     self.lbl_tab_2_line = tkinter.Label(self.frame_3_l_bdy, text='路線名')
    #     self.lbl_tab_2_line.grid(row=0, column=1)
    #     self.frame_tab_2_line = tkinter.Frame(self.frame_3_l_bdy)
    #     self.frame_tab_2_line.grid(row=1, column=1)
    #     self.lst_tab_2_line = tkinter.Listbox(
    #         self.frame_tab_2_line, listvariable=self.lst_line)
    #     self.lst_tab_2_line.pack(side=tkinter.LEFT, fill="y")
    #     self.scroll_lst_tab_2_line = tkinter.Scrollbar(
    #         self.frame_tab_2_line, orient=tkinter.VERTICAL, command=self.lst_tab_2_line.yview)
    #     self.scroll_lst_tab_2_line.pack(side=tkinter.RIGHT, fill="y")
    #     self.lst_tab_2_line["yscrollcommand"] = self.scroll_lst_tab_2_line.set
    #     self.btn_tab_2_line = tkinter.Button(self.frame_3_l_bdy, text='絞り込み')
    #     self.btn_tab_2_line.configure(state=tkinter.DISABLED)
    #     self.btn_tab_2_line.grid(row=3, column=1)

    #     self.lbl_tab_2_sta = tkinter.Label(self.frame_3_l_bdy, text='駅名')
    #     self.lbl_tab_2_sta.grid(row=0, column=2)
    #     self.frame_tab_2_sta = tkinter.Frame(self.frame_3_l_bdy)
    #     self.frame_tab_2_sta.grid(row=1, column=2)
    #     self.lst_tab_2_sta = tkinter.Listbox(
    #         self.frame_tab_2_sta, listvariable=self.lst_sta)
    #     self.lst_tab_2_sta.pack(side=tkinter.LEFT, fill="y")
    #     self.scroll_lst_tab_2_sta = tkinter.Scrollbar(
    #         self.frame_tab_2_sta, orient=tkinter.VERTICAL, command=self.lst_tab_2_sta.yview)
    #     self.scroll_lst_tab_2_sta.pack(side=tkinter.RIGHT, fill="y")
    #     self.lst_tab_2_sta["yscrollcommand"] = self.scroll_lst_tab_2_sta.set
    #     self.btn_tab_2_sta = tkinter.Button(self.frame_3_l_bdy, text='絞り込み')
    #     self.btn_tab_2_sta.configure(state=tkinter.DISABLED)
    #     self.btn_tab_2_sta.grid(row=3, column=2)

    #     self.lbl_tab_2_track = tkinter.Label(self.frame_3_l_bdy, text='番線')
    #     self.lbl_tab_2_track.grid(row=0, column=3)
    #     self.frame_tab_2_track = tkinter.Frame(self.frame_3_l_bdy)
    #     self.frame_tab_2_track.grid(row=1, column=3)
    #     self.lst_tab_2_track = tkinter.Listbox(
    #         self.frame_tab_2_track, listvariable=self.lst_track)
    #     self.lst_tab_2_track.pack(side=tkinter.LEFT, fill="y")
    #     self.scroll_lst_tab_2_track = tkinter.Scrollbar(
    #         self.frame_tab_2_track, orient=tkinter.VERTICAL, command=self.lst_tab_2_track.yview)
    #     self.scroll_lst_tab_2_track.pack(side=tkinter.RIGHT, fill="y")
    #     self.lst_tab_2_track["yscrollcommand"] = self.scroll_lst_tab_2_track.set
    #     self.btn_tab_2_track = tkinter.Button(self.frame_3_l_bdy, text='絞り込み')
    #     self.btn_tab_2_track.configure(state=tkinter.DISABLED)
    #     self.btn_tab_2_track.grid(row=3, column=3)

    def active_tab_3(self):
        self.int_tab_no.set(3)
        self.reflesh_tab()
        self.tab_3.configure(background=cl_active)

        self.frame_3_l_bdy = tkinter.LabelFrame(
            self.frame_3_l, width=size_frame3l_w, height=size_frame3l_h)
        self.frame_3_l_bdy.grid(row=1, column=0)
        self.frame_3_l_bdy.configure(background=cl_active)

        self.lbl_tab_3_mel = tkinter.Label(self.frame_3_l_bdy, text='メロディー',font=("", size_font_h4))
        self.lbl_tab_3_mel.grid(row=0, column=0)
        self.frame_tab_3_mel = tkinter.Frame(self.frame_3_l_bdy)
        self.frame_tab_3_mel.grid(row=1, column=0)
        self.lst_tab_3_mel = tkinter.Listbox(
            self.frame_tab_3_mel, exportselection=0, listvariable=self.lst_mel, width=size_frame3l_tab3_box_w, height=size_frame3l_tab3_box_h)
        self.lst_tab_3_mel.pack(side=tkinter.LEFT, fill="y")
        self.scroll_lst_tab_3_mel = tkinter.Scrollbar(
            self.frame_tab_3_mel, orient=tkinter.VERTICAL, command=self.lst_tab_3_mel.yview)
        self.scroll_lst_tab_3_mel.pack(side=tkinter.RIGHT, fill="y")
        self.lst_tab_3_mel["yscrollcommand"] = self.scroll_lst_tab_3_mel.set

        self.lbl_tab_3_ann = tkinter.Label(self.frame_3_l_bdy, text='アナウンス',font=("", size_font_h4))
        self.lbl_tab_3_ann.grid(row=0, column=1)
        self.frame_tab_3_ann = tkinter.Frame(self.frame_3_l_bdy)
        self.frame_tab_3_ann.grid(row=1, column=1)
        self.lst_tab_3_ann = tkinter.Listbox(
            self.frame_tab_3_ann, exportselection=0, listvariable=self.lst_ann, width=size_frame3l_tab3_box_w, height=size_frame3l_tab3_box_h)
        self.lst_tab_3_ann.pack(side=tkinter.LEFT, fill="y")
        self.scroll_lst_tab_3_ann = tkinter.Scrollbar(
            self.frame_tab_3_ann, orient=tkinter.VERTICAL, command=self.lst_tab_3_ann.yview)
        self.scroll_lst_tab_3_ann.pack(side=tkinter.RIGHT, fill="y")
        self.lst_tab_3_ann["yscrollcommand"] = self.scroll_lst_tab_3_ann.set

    def use_sw(self):
        self.prev_sw = 'off'
        while(True):
            if(self.env.connect_sw and self.flg_swopt.get()):
                if(GPIO.input(24) == 0):
                    if(self.prev_sw == 'on'):
                        self.set_sw_off()
                    self.prev_sw = 'off'
                else:
                    if(self.prev_sw == 'off'):
                        self.set_sw_on()
                    self.prev_sw = 'on'

    def raspi(self):
        try:
            import RPi.GPIO as GPIO

            GPIO.setmode(GPIO.BCM)
            GPIO.setup(24, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
            self.th_gpio = threading.Thread(target=self.use_sw)
            self.th_gpio.start()

        except Exception as e:
            print(e)


if __name__ == '__main__':
    w = Window()
    w.pack(anchor='center', expand=11)
    w.mainloop()
