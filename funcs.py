import simpleaudio
import subprocess
import threading
import wave
import contextlib
import time
import random
import csv


def get_sound_len(m):
    with contextlib.closing(wave.open(m, 'r'))as f:
        frames = f.getnframes()
        rate = f.getframerate()
        length = frames / float(rate)
        print(length)
        return length


class SoundManager():
    def __init__(self):
        self.filepath = None
        self.time_file_len = 0
        self.wav_obj = None
        self.wav_obj_player = None
        self.time_memo = 0
        self.time_wait = 0
        self.flg_loop = False
        self.th_play_sound = None
        self.replay_counter = 0
        self.btn_on = False
        self.th_play_sound = threading.Thread(target=self.play_melody)
        self.th_play_all = threading.Thread(target=self.play_all)

    def add_file(self, filepath):
        print('in def add_file()')
        self.filepath = filepath
        self.time_file_len = get_sound_len(self.filepath)
        self.wav_obj = simpleaudio.WaveObject.from_wave_file(self.filepath)

    def start_play(self, time_to_wait, replay_flg=False):
        print('in def start_play()')
        self.btn_on = True
        self.time_wait = time_to_wait
        self.flg_loop = replay_flg
        self.th_play_all = threading.Thread(target=self.play_all)
        self.th_play_all.start()

    def play_all(self):
        print('in def play_all()')
        self.th_play_sound = threading.Thread(target=self.play_melody)
        self.th_play_sound.start()

    def play_melody(self):
        print('in def play_melody()')
        self.play_silent()
        self.wav_obj_player = self.wav_obj.play()
        self.time_memo = time.time()
        while((self.btn_on)):
            if(time.time()-self.time_memo > self.time_file_len):
                self.wav_obj_player.stop()
                self.th_play_sound._stop
                if(self.flg_loop):
                    self.play_loop()
                break
        self.wav_obj_player.stop()
        self.th_play_sound._stop
        print('抜けてるかチェック')

    def play_silent(self):
        print('in def play_silent()')
        self.time_memo = time.time()
        while((self.btn_on)):
            if(time.time()-self.time_memo > self.time_wait):
                break
        return 0

    def play_loop(self):
        print('in def play_loop()')
        self.play_all()

    def stop_play(self):
        print('in def stop_play()')
        self.btn_on = False
        self.th_play_sound._stop
        self.th_play_all._stop
        if(self.wav_obj_player != None):
            self.wav_obj_player.stop()


class EnvManager():
    def __init__(self):
        self.melody_path = ''
        self.melody_name = ''
        self.melody_wait = 0
        self.melody_loop = True

        self.announce_path = ''
        self.announce_name = ''
        self.announce_wait = 0
        self.announce_loop = False

        self.list_path_melody = []
        self.list_path_announce = []

        self.play_rand = False

        self.connect_sw = True
        self.btn_on_off = 'off'

    def set_sound(
            self,
            melody_path,
            melody_name,
            announce_path,
            announce_name,):

        self.melody_path = melody_path
        self.melody_name = melody_name
        self.announce_path = announce_path
        self.announce_name = announce_name


    def set_env(
            self,
            melody_wait,
            melody_loop,
            announce_wait):

        self.melody_wait = melody_wait
        self.melody_loop = melody_loop
        self.announce_wait = announce_wait

    def set_btn_on_off(self,st):
        if(st=='on'):
            self.btn_on_off='on'
        if(st=='off'):
            self.btn_on_off='off'

    def get_btn_on_off(self):
        if(self.connect_sw):
            self.btn_on_off = 'sw'
        return self.btn_on_off


    def set_list_path(self,list_path_melody,list_path_announce):
        self.list_path_melody = list_path_melody
        self.list_path_announce = list_path_announce

    def set_rand(self):
        self.melody_path=self.list_path_melody[int(random.randrange(len(self.list_path_melody)))]
        self.announce_path=self.list_path_announce[int(random.randrange(len(self.list_path_announce)))]




class ManageCsv():
  def __init__(self):
    self.csv_listid= []
    self.csv_corp= []
    self.csv_line= []
    self.csv_sta= []
    self.csv_track= []
    self.csv_mel_name = []
    self.csv_mel_path = []
    self.csv_mel_delay = []
    self.csv_ann_name = []
    self.csv_ann_path = []
    self.csv_ann_delay = []
    self.csv_flg_loop = []

    self.csv_readall = []

  def csv_read(self,path_to_csv):
    csv_file = open(path_to_csv, "r", encoding="utf-8", errors="", newline="" )
    #リスト形式
    r = csv.reader(csv_file, delimiter=",", doublequote=True, lineterminator="\r\n", quotechar='"', skipinitialspace=True)

    for row in r:
      self.csv_readall.append(row)
      self.csv_listid.append(row[0])
      self.csv_corp.append(row[1])
      self.csv_line.append(row[2])
      self.csv_sta.append(row[3])
      self.csv_track.append(row[4])
      self.csv_mel_name.append(row[5])
      self.csv_mel_path.append(row[6])
      self.csv_mel_delay.append(row[7])
      self.csv_ann_name.append(row[8])
      self.csv_ann_path.append(row[9])
      self.csv_ann_delay.append(row[10])
      self.csv_flg_loop.append(row[11])
    # print(self.csv_ann_name)

  def get_name_mel(self):
    return self.csv_mel_name

  def get_name_ann(self):
    return self.csv_ann_name

  def get_glp(self):
    ret = []
    for r in self.csv_readall:
      ret.append([r[1]+", "+r[2]+", "+r[3]+", "+r[4]+", "+r[5]+", "+r[8]])
    return ret

  def get_mel_path_by_index(self,num):
    return self.csv_mel_path[num]

  def get_ann_path_by_index(self,num):
    return self.csv_ann_path[num]
    
  def get_mel_name_by_index(self,num):
    return self.csv_mel_name[num]

  def get_ann_name_by_index(self,num):
    return self.csv_ann_name[num]
    
  def get_sta_name_by_index(self,num):
    return self.csv_sta[num]
    
