情報通信プロジェクト 課題
# Raspiで発車ベルを鳴らす
鉄道の駅などで使用されている発車ベル装置の再現ツール  
Pythonを使用しているため、OSに依存することなく使用できるのが特徴  
RaspberryPiであれば外部スイッチを使用することが可能  
**音源は自分で用意してください**  

# 外観
<img src=img/image1.png width=400 />


## 動作確認環境
Windows 10 / MacOS Catalina / Raspberry Pi OS with desktop   
Python 3.7.6   
Raspberry Pi (外部スイッチを使用する場合)    

## 使い方
### 導入
simpleaudioとTkinterを使用  
Tkinterは基本入っていますが、たまに用意されていないのでimportでエラーが出る場合は手動で入れてください  
外部スイッチを接続する場合はRPi.GPIOが必要(Raspberry Pi)  
``` sh:install.sh
$ pip install simpleaudio
# clone
$ git clone https://gitlab.com/ykst/ekimelo_player.git
```
必要に応じて 
``` sh:setup.sh
$ pip install python-tk
$ pip install RPi.GPIO
```
実行
``` sh:run.sh
$ cd ekimelo_player
# run
$ python main.py
```

### 画面の説明

<img src=img/image2.png width=400 />

##### 1 再生中ファイルの表示
ランダム再生, パス指定 : メロディーとアナウンスにはファイルパスが表示されます  
駅指定, メロディー/アナウンス指定 : メロディーとアナウンスには名称が表示されます  

##### 2 設定
ランダム  
TRUE : ランダム再生ON  
FALSE : ランダム再生OFF  
  
ループ  
TRUE : メロディーがループします  
FALSE : メロディーがループしません    
  
SW接続  
Active : スイッチ接続時にスイッチを使用できるようにします  
Passive : スイッチ接続時にスイッチを使用不可(切断)にします  

##### 3,4 ファイル選択
パス指定  
メロディー : ekimelo/ 内のファイルを選択可能  
アナウンス : announce/ 内のファイルを選択可能  
  
駅指定  
ekilist.csvに指定したメロディーとアナウンスの組み合わせを再生します  
例 : [東京駅 1番線] のメロディーと [東京駅 1番線] のアナウンス  
  
メロディー/アナウンス指定  
ekilist.csvに指定したメロディーとアナウンスをそれぞれ指定します  
例 : [東京駅 1番線] のメロディーと [新宿駅 2番線] のアナウンス  


##### 5 再生スイッチ
ON を押下するとメロディーが再生されます  
OFF を押下するとアナウンスが再生されます  
SW接続中に再生スイッチを操作すると誤作動を起こします  

## 外部スイッチの接続
### 必要なもの
* スイッチ(動作確認 : 春日電機 動力用開閉器 BSW215B3)  
* ジャンパ線(接続用ケーブル)  
* 工具(ドライバー ニッパー 等)  

### 配線(Raspberry Pi)
3.3V(17番)とGPIO(18番)を使用  
短絡した場合にスイッチONとなります  
Rasberry Piと接続する場合、抵抗は挟まなくても動作します  

### 接続例
<img src=img/image3.png width=400 />

## ファイルの構成
### 概要
```
announce/
# アナウンスの音源
# ここに配置することでランダム再生時にアナウンスで選択されます

ekimelo/
# 発車ベル, アナウンス の音源
# ここに配置することでランダム再生時にメロディーで選択されます

ekilist.csv
# リスト化する場合に使用するファイル

funcs.py
# 本体

main.py
# 本体(実行する)

none.wav
# 無音音源

img/
# README.md の画像

README.md
# このREADME

LICENSE
# ライセンス
```

### ekilist.csv  (サンプル)
./none.wavは用意されています(無音音源(5秒))  
それ以外はご自身で用意してください  
再生時にエラーが出るのでメロディーパスorアナウンスパスで指定できない場合は ./none.wav を指定してください。  
``` csv:ekilist.csv
ID, 会社名,     路線名, 駅,     ホーム, メロディー名称,  メロディーパス,            メロディー待ち,     アナウンス名称,   アナウンスパス,                 アナウンス待ち,  メロディーループ
0,  テスト鉄道, 南線,   駅1,    1,      Example,        ./ekimelo/example.wav,   0,                 Example,        ./announce/example.wav,       0,              1
1,  テスト鉄道, 西線,   駅1,    2,      Example,        ./ekimelo/example.wav,   0,                 Example,        ./announce/example.wav,       0,              1
2,  実験鉄道,   東線,   駅1,    3,      Example,        ./ekimelo/example.wav,   0,                 -,              ./none.wav,                   0,              1
3,  実験鉄道,   東線,   駅2,    1,      -,              ./none.wav,              0,                 Example,        ./announce/example.wav,       0,              1
```


## Licence
[MIT Licence](LICENSE)
